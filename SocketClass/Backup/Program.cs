﻿//Сокеты
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;

class Program
{
    static void Main(string[] args)
    {
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); //Создаем основной сокет
        IPAddress ipAddress = null; //IP-адресс
        IPEndPoint Addr = null; //конечная точка(IP и порт)

        Console.WriteLine("1 - Server\n2 - Client");
        switch (Console.ReadKey(true).KeyChar)
        {
            case '1':
                Console.Title = "Server"; //Сервер
                //ipAddress = Dns.GetHostEntry("localhost").AddressList[0]; //Преобразовуем адрес или IP узла в массив IP и берем первый
                Addr = new IPEndPoint(IPAddress.Any, 5300); //"localhost" = 127.0.0.1
                s.Bind(Addr); //"Адресуем" сокет
                s.Listen(10); //Обозначаем количество ожидающих в очереди на подключение
                Console.WriteLine("Ждем коннекта...");
                Socket cl_s = s.Accept(); //Ожидаем подключения
                Console.WriteLine("Есть коннект!");
                while (!Console.KeyAvailable) //Пока не нажата клавиша
                {
                    byte[] msg = new byte[cl_s.Available];
                    cl_s.Receive(msg); //Принимаем МСГ
                    Console.WriteLine(Encoding.UTF8.GetString(msg)); //Конвертируем и выводим
                }
                cl_s.Close(); //Закрываем сокет
                break;
            case '2':
                Console.Title = "Client"; //Клиент
                ipAddress = Dns.GetHostEntry("localhost").AddressList[0];
                Addr = new IPEndPoint(ipAddress, 5300); //"localhost" = 127.0.0.1
                s.Connect(Addr); //Коннектимся к срверу
                while (true) //Вечная истина :)
                {
                    byte[] msg = Encoding.UTF8.GetBytes(Console.ReadLine()); //Конвертируем
                    s.Send(msg); //Отправляем
                }
                break;
        }
        s.Close(); //Закрываем сокет
        Console.ReadKey();
    }
}

//string st = "localhost";
//IPAddress hostIPAddress1 = (Dns.GetHostEntry(st)).AddressList[0];
//Console.WriteLine(hostIPAddress1.ToString());
//IPEndPoint hostIPEndPoint = new IPEndPoint(hostIPAddress1, 80);
//Console.WriteLine("\nIPEndPoint information:" + hostIPEndPoint.ToString());
//Console.WriteLine("\n\tMaximum allowed Port Address :" + IPEndPoint.MaxPort);
//Console.WriteLine("\n\tMinimum allowed Port Address :" + IPEndPoint.MinPort);
//Console.WriteLine("\n\tAddress Family :" + hostIPEndPoint.AddressFamily);
//Console.WriteLine((Dns.GetHostEntry(st)).HostName);